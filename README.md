# Resources

This repository contains supporting resources for my [YouTube Channel](https://adrian.goins.tv). 

- [E11](resources/E11) - Kubernetes 101: Why You Need To Use MetalLB
- [2020.01](resources/2020.01) - Using MetalLB with Nginx and Rancher
- [2020.02](resources/2020.02) - K8s Chaos Engineering with KubeInvaders
- [2020.06](resources/2020.06) - Secure Wordpress on Kubernetes
- [2021.02](resources/2021.02-ha-k3s-kube-vip-metallb) - HA K3s With etcd, kube-vip, MetalLB, and Rancher!
